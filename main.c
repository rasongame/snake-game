#include "main.h"
#include <curses.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
void spawn_food(struct player_t* player, struct field_t* field)
{
    mvaddch(field->food_pos[0], field->food_pos[1], ' ');
    field->food_pos[0] = random_number(1, getmaxy(stdscr) - 2); // generate from 1, 1 to field borders( width , height )
    field->food_pos[1] = random_number(1, getmaxx(stdscr) - 2);
    attron(COLOR_PAIR(4));
    mvaddch(field->food_pos[0], field->food_pos[1], fruit_sym);
    attron(COLOR_PAIR(4));
}
bool eaten()
{
    if ((player.x == field.food_pos[1]) && (player.y == field.food_pos[0])) {
        spawn_food(&player, &field);

        return true;
    }
    return false;
}
void update(struct player_t* player)
{
    // update game state;
    if (eaten()) {
        player->eated++;
    }
    switch (player->last_pressed_key) {
    case KEY_LEFT:
        player->side = side_left;
        break;
    case KEY_RIGHT:
        player->side = side_right;
        break;
    case KEY_DOWN:
        player->side = side_down;
        break;
    case KEY_UP:
        player->side = side_up;
        break;
    case 'c':
        spawn_food(player, &field);
        break;
    case 'q':
        endwin();
        exit(0);
        break;
    }
}

int random_number(int min_num, int max_num)
{
    int result = 0, low_num = 0, hi_num = 0;

    if (min_num < max_num) {
        low_num = min_num;
        hi_num = max_num + 1; // include max_num in output
    } else {
        low_num = max_num + 1; // include max_num in output
        hi_num = min_num;
    }

    srand(time(NULL));
    result = (rand() % (hi_num - low_num)) + low_num;
    return result;
}

void render(struct player_t* player)
{
    int x = player->x;
    int y = player->y;
    switch (player->side) {
    case side_left:
        if (player->x >= 1) {
            player->x--;
        } else {
            player->failed = true;
        }
        break;
    case side_right:
        if (player->x <= getmaxx(stdscr) - 2) {
            player->x++;
        } else {
            player->failed = true;
        }
        break;
    case side_down:
        if (player->y <= getmaxy(stdscr) - 4) {
            player->y++;
        } else {
            player->failed = true;
        }
        break;
    case side_up:
        if (player->y >= 1) {
            player->y--;
        } else {

            player->failed = true;
        }
        break;
    }
    mvaddch(y, x, ' ');
    attron(COLOR_PAIR(4));
    mvaddch(player->y, player->x, player_sym);
    attroff(COLOR_PAIR(4));
}
void init()
{

    player.side = side_down;
    player.x = 0;
    player.y = 0;
    player.failed = false;
    initscr();
    cbreak();
    noecho();
    curs_set(0);
    keypad(stdscr, true);
    start_color();
    init_pair(4, COLOR_BLACK, COLOR_WHITE);

    spawn_food(&player, &field);
}
void clearln(int num, char sym)
{
    for (int i = 0; i < getmaxx(stdscr); i++) {
        mvaddch(num, i, sym);
    }
}
int main(int argc, char* argv[])
{

    init();
    timeout(100); // need for .. endless run
    while (!player.failed) {

        player.last_pressed_key = getch();
        update(&player);
        render(&player);
        clearln(getmaxy(stdscr) - 2, ' ');
        clearln(getmaxy(stdscr) - 1, ' ');
        mvprintw(getmaxy(stdscr) - 2, 1, "x: %d x: %d; resolution = %d %d", player.x, player.y, getmaxx(stdscr), getmaxy(stdscr));
        mvprintw(getmaxy(stdscr) - 1, 1, "food x, y  : %d %d === coins: %d ", field.food_pos[1], field.food_pos[0], player.eated);
        refresh();
    }
    printf("You earned %d fruits!..", player.eated);
    endwin();
    return 0;
}
