#include <ncurses.h>
#include <stdlib.h>
#include <stdio.h>
bool eaten();
int random_number(int min_num, int max_num);
void clearln(int num, char sym);
void init();
enum sides {
	side_left,
	side_right,
	side_up,
	side_down 
};
char fruit_sym = 'a';
char player_sym = '@';
char tail_sym = '~';
int ch = 0;
struct field_t {
	int w, h;
	int food_pos[2]; // w , h 
} field = {0,0,{999,999}};
struct player_t {
	int side, eated;
	int tail_pos[128][2];
	int x, y;
	int last_pressed_key;
	bool failed;
} player;



